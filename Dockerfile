FROM alpine:3.17.0

RUN apk --update --no-cache add bash curl net-tools wget vim bash-completion git helm python3
RUN python -m ensurepip --upgrade && \
    pip3 install --upgrade pip && \
    pip3 install semver
COPY bash_profile /etc/profile
RUN sed -i '1s/ash/bash/' /etc/passwd
ENTRYPOINT ["/bin/bash", "--login"]